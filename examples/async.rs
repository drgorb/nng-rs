//! A simple asynchronous RPC server example.
//!
//! This example shows how to write an asynchronous RPC server using Contexts.
//! This is heavily based on the `nng` demonstration program of the same name.
//!
//! The protocol is simple: the client sends a request with the number of
//! milliseconds to wait, the server waits that long and sends back an empty
//! reply.
extern crate bincode;
extern crate serde;
#[macro_use]
extern crate serde_derive;

use std::{env, process, thread};
use std::io::Write;
use std::time::{Duration, Instant, UNIX_EPOCH, SystemTime};

use nng::{Aio, AioResult, Context, Message, Protocol, Socket};
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

/// Number of outstanding requests that we can handle at a given time.
///
/// This is *NOT* the number of threads in use, but instead represents
/// outstanding work items. Select a small number to reduce memory size. (Each
/// one of these can be thought of as a request-reply loop.) Note that you will
/// probably run into limitations on the number of open file descriptors if you
/// set this too high. (If not for that limit, this could be set in the
/// thousands, each context consumes a couple of KB.)
const PARALLEL: usize = 1024 * 1024;

#[derive(Debug, Serialize, Deserialize)]
struct TestMessage {
    origin: usize,
    server: usize,
    message: String,
    ms: u64,
}

impl TestMessage {
    fn to_byte_vec(&self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }

    fn to_nng_msg(&self) -> Message {
        let mut msg = Message::new().expect("could not create message");
        msg.write(self.to_byte_vec().as_slice()).expect("failed to write message");
        msg
    }

    pub fn from_bytes(bytes: &[u8]) -> TestMessage {
        bincode::deserialize(bytes).unwrap()
    }
}

/// Entry point of the application.
fn main() -> Result<(), nng::Error> {
    // Begin by parsing the arguments. We are either a server or a client, and
    // we need an address and potentially a sleep duration.
    let args: Vec<_> = env::args().collect();

    match &args[..] {
        [_, t, url] if t == "server" => server(url),
        [_, t, url, count] if t == "client" => client(url, count.parse().unwrap()),
        _ => {
            println!("Usage:\nasync server <url>\n  or\nasync client <url> <ms>");
            process::exit(1);
        }
    }
}

/// Run the client portion of the program.
fn client_worker_callback(aio: Aio, ctx: &Context, res: AioResult, idx: usize) {
    match res {
        // We successfully sent the message, wait for a new one.
        AioResult::Send(Ok(_)) => ctx.recv(&aio).unwrap(),

        // We successfully received a message.
        AioResult::Recv(Ok(m)) => {
            let dur = SystemTime::now().duration_since(UNIX_EPOCH).expect("could not get time");
            let subsecs: u64 = dur.subsec_millis().into();

            let mut msg: TestMessage = TestMessage::from_bytes(m.as_slice());
            println!("{:?} elapse time {}", msg, dur.as_secs() * 1000 + subsecs - msg.ms);
            aio.sleep(Duration::from_millis(2000)).unwrap();
        }

        // We successfully slept.
        AioResult::Sleep(Ok(_)) => {
            let dur = SystemTime::now().duration_since(UNIX_EPOCH).expect("could not get time");
            let subsecs: u64 = dur.subsec_millis().into();
            let msg: TestMessage = TestMessage {
                origin: idx,
                server: 0,
                message: thread_rng()
                    .sample_iter(&Alphanumeric)
                    .take(40)
                    .collect(),
                ms: dur.as_secs() * 1000 + subsecs,
            };
//            println!("{:?}", msg);
            ctx.send(&aio, msg.to_byte_vec().as_slice()).unwrap();
        }

        // Anything else is an error and we will just panic.
        AioResult::Send(Err((_, e))) | AioResult::Recv(Err(e)) | AioResult::Sleep(Err(e)) => {
            panic!("Error: {}", e)
        }
    }
}

/// Run the server portion of the program.
fn client(url: &str, ms: u64) -> Result<(), nng::Error> {
    // Create the socket
    let s = Socket::new(Protocol::Req0)?;

    // Create all of the worker contexts
    let workers: Vec<_> = (0..PARALLEL)
        .map(|i| {
            let ctx = Context::new(&s)?;
            let ctx_clone = ctx.clone();
            let aio = Aio::new(move |aio,
                                     res| client_worker_callback(aio, &ctx_clone, res, i + 1))?;
            Ok((aio, ctx))
        })
        .collect::<Result<_, nng::Error>>()?;

    // Only after we have the workers do we dial in.
    s.dial(url)?;

    // Now start all of the workers sending.
    for (a, c) in &workers {
        a.sleep(Duration::from_millis(100));
    }

    thread::sleep(Duration::from_secs(30));

    Ok(())
}

fn server(url: &str) -> Result<(), nng::Error> {
    // Create the socket
    let s = Socket::new(Protocol::Rep0)?;

    // Create all of the worker contexts
    let workers: Vec<_> = (0..PARALLEL)
        .map(|i| {
            let ctx = Context::new(&s)?;
            let ctx_clone = ctx.clone();
            let aio = Aio::new(move |aio,
                                     res| worker_callback(aio, &ctx_clone, res, i + 1))?;
            Ok((aio, ctx))
        })
        .collect::<Result<_, nng::Error>>()?;

    // Only after we have the workers do we start listening.
    s.listen(url)?;

    // Now start all of the workers listening.
    for (a, c) in &workers {
        c.recv(a)?;
    }

    thread::sleep(Duration::from_secs(60 * 60 * 24 * 365));

    Ok(())
}

/// Callback function for workers.
fn worker_callback(aio: Aio, ctx: &Context, res: AioResult, idx: usize) {
    match res {
        // We successfully sent the message, wait for a new one.
        AioResult::Send(Ok(_)) => ctx.recv(&aio).unwrap(),

        // We successfully received a message.
        AioResult::Recv(Ok(m)) => {
            let mut msg: TestMessage = TestMessage::from_bytes(m.as_slice());
            msg.server = idx;
//            aio.sleep(Duration::from_millis(_msg.ms)).unwrap();
            ctx.send(&aio, msg.to_byte_vec().as_slice()).unwrap();
        }

        // We successfully slept.
        AioResult::Sleep(Ok(_)) => {}

        // Anything else is an error and we will just panic.
        AioResult::Send(Err((_, e))) | AioResult::Recv(Err(e)) | AioResult::Sleep(Err(e)) => {
            panic!("Error: {}", e)
        }
    }
}
